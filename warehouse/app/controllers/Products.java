package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Product;
import models.StockItem;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import com.avaje.ebean.Page;

public class Products extends Controller {

	private static final Form<Product> productForm = Form.form(Product.class);

	public static Result list(Integer page) {
		// final List<Product> product = Product.findAll();
		Page<Product> products = Product.find(page);
		return ok(views.html.list.render(products));
	}

	public static Result newProduct() {
		return ok(views.html.details.render(productForm));
	}

	public static Result details(Product product) {
		if (product == null) {
			return notFound(String.format("Product %s does not exist.",
					product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(views.html.details.render(filledForm));
	}

	public static Result save() {
		Form<Product> boundForm = productForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(views.html.details.render(boundForm));
		}
		Product product = boundForm.get();
		// Ebean.save(product);
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;

		if (product.id == null) {
			product.save();
			StockItem stockItem = new StockItem();
			stockItem.product = product;
			stockItem.quantity = 0L;
			Product mProduct = Product.findByEan(product.ean);
			stockItem.save();
		}

		// Ebean.save(pro      duct);
		else
			product.update();
		// Ebean.update(product);

		// Ebean.save(product);
		flash("succes", String.format("Successfully added list", product));
		return redirect(routes.Products.list(0));
	}

	public static Result delete(String ean) {
		final Product product = Product.findByEan(ean);
		if (product == null) {
			return notFound("Product not found");
		}
		// product.delete();

		product.delete();
		return redirect(routes.Products.list(0));
	}

}
