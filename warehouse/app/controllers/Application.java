package controllers;

import static play.data.Form.form;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.index;

@Security.Authenticated(Secured.class)
public class Application extends Controller {

	public static Result index() {
		return ok(index.render("Your new application is ready."));
	}

	public static class Login {
		public String email;
		public String password;
	}
	public static Result authenticate(){
		Form<Login> loginForm=form(Login.class).bindFromRequest();
		String email=loginForm.get().email;
		String password=loginForm.get().password;
		
		session().clear();
		if(UserAccount.authenticate(email, password)==null){
			flash("error","Invalid email and/or password");
			return redirect(routes.Application.login());
		}
		session("email",email);
		return redirect(routes.Products.list(0));
	}
	public static Result login(){
		return ok(views.html.login.render(form(Login.class)));
	}

}
