package controllers;

import play.mvc.Result;
import play.mvc.Security;
import play.mvc.Http.Context;

public class Secured extends Security.Authenticator {
	@Override
	public String getUsername(Context arg0) {
		// TODO Auto-generated method stub
		return arg0.session().get("email");
	}

	@Override
	public Result onUnauthorized(Context arg0) {
		// TODO Auto-generated method stub
		return redirect(routes.Application.login());
	}

}
