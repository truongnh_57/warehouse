package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import com.avaje.ebean.Page;

@Entity
public class Product extends Model implements PathBindable<Product> {
	private static List<Product> products;
	static {
		products = new ArrayList<Product>();
		products.add(new Product("1111111111", "mot", "motF"));
		products.add(new Product("2222222222", "hai", "hai"));
		products.add(new Product("3333333333", "ba", "ba"));
	}

	@Constraints.Required
	public String ean;

	@Id
	public Long id;

	@Constraints.Required
	public String name;

	public String description;
	public Date date;
	@OneToMany(mappedBy = "product")
	public List<StockItem> stockItem;
	@ManyToMany
	public List<Tag> tags;
	public byte[] picture;

	public Product(String ean, String name, String description) {
		super();
		this.ean = ean;
		this.name = name;
		this.description = description;
	}

	public Product() {
		super();
	}

	public String toString() {
		return ean + " - " + name;
	}

	public static Finder<Long, Product> find = new Finder<Long, Product>(
			Long.class, Product.class);

	public static List<Product> findAll() {
		return find.all();
	}

	public static Product findByEan(String ean) {
		return find.where().eq("ean", ean).findUnique();
	}

	public static List<Product> findByName(String name) {
		List<Product> results = new ArrayList<Product>();
		for (Product candicate : products) {
			if (candicate.name.toLowerCase().equals(name.toLowerCase()))
				results.add(candicate);
		}
		return results;
	}

	@Override
	public Product bind(String key, String value) {
		return findByEan(value);
	}

	@Override
	public String unbind(String key) {
		return ean;
	}

	@Override
	public String javascriptUnbind() {
		return ean;
	}

	public static Page<Product> find(int page) {
		return find.where().orderBy("id asc").findPagingList(10)
				.setFetchAhead(false).getPage(page);
	}
}
