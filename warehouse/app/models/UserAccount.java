package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
public class UserAccount extends Model {
	@Id
	public Long id;
	@Constraints.Required
	public String email;
	@Constraints.Required
	public String password;

	public UserAccount() {
		super();
	}

	public UserAccount(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public static Finder<Long, UserAccount> finder = new Finder<Long, UserAccount>(
			Long.class, UserAccount.class);

	public static UserAccount authenticate(String email, String password) {
		return finder.where().eq("email", email).eq("password", password).findUnique();
	}

}
